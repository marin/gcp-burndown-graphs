"use strict";

var es = require("event-stream");
var urlStreamer = require("./url-streamer");
var Promise = require("bluebird");

function collectResults(url, stopper) {
  let collect = [];
  return new Promise((resolve, reject) => {
    urlStreamer(url, stopper)
      .pipe(
        es.map(function(issue, callback) {
          collect.push(issue);
          callback(null, issue);
        })
      )
      .on("error", reject)
      .on("end", function() {
        resolve(collect);
      });
  });
}

module.exports = collectResults;
