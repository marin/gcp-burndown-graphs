"use strict";

var es = require("event-stream");
var urlStreamer = require("./url-streamer");
var appendSheet = require("./append-sheet");
var Promise = require("bluebird");

var googleDate = new Date()
  .toISOString()
  .replace("T", " ")
  .replace("Z", "");

class Categoriser {
  constructor(labels, defaultCategory) {
    this.labelsHash = labels.reduce((memo, l) => {
      memo[l] = true;
      return memo;
    }, {});
    this.defaultCategory = defaultCategory;

    this.counts = labels.reduce(
      (memo, l) => {
        memo[l] = 0;
        return memo;
      },
      { [defaultCategory]: 0 }
    );

    this.scores = labels.reduce(
      (memo, l) => {
        memo[l] = 0;
        return memo;
      },
      { [defaultCategory]: 0 }
    );
  }

  score(issue) {
    if (issue.state === "closed") return;

    let labels = issue.labels;
    let weight = issue.weight || 0;

    if (!labels || !labels.length) {
      this.counts[this.defaultCategory]++;
      this.scores[this.defaultCategory] += weight;
      return;
    }

    labels.forEach(label => {
      if (this.labelsHash[label]) {
        this.counts[label]++;
        this.scores[label] += weight;
      }
    });
  }
}

function processIssue(url, processor) {
  let count = 0;
  console.log("Processing issues");

  return new Promise((resolve, reject) => {
    urlStreamer(url)
      .pipe(
        es.map(function(issue, callback) {
          count++;
          processor(issue);
          callback(null, issue);
        })
      )
      .on("error", reject)
      .on("end", function() {
        console.log(`Processed ${count} issues`);
        resolve();
      });
  });
}

async function reportBurndown() {
  var burndown = new Categoriser(
    [
      "Planning",
      "Ready",
      "blocked",
      "In Progress",
      "Premigration",
      "Postmigration",
      "Workstream: Failover Testing",
      "Workstream: Logging and Monitoring",
      "Workstream: Post Failover",
      "Workstream: Staging",
      "Team:Production",
      "Team:Geo",
      "Team:Security",
      "Team:Quality"
    ],
    "Other"
  );

  await processIssue("https://gitlab.com/api/v4/projects/gitlab-com%2fmigration/issues?state=opened&scope=all&labels=Premigration", issue => {
    burndown.score(issue);
  });

  let weightRow = Object.values(burndown.scores);
  weightRow.unshift(googleDate);
  await appendSheet("1H9h5fLzGpOkdXnledNWG0-_8iaTss1Cb9K784qr8qTs", "Weight_Tracker!A:O", weightRow);

  let countRow = Object.values(burndown.counts);
  countRow.unshift(googleDate);
  await appendSheet("1H9h5fLzGpOkdXnledNWG0-_8iaTss1Cb9K784qr8qTs", "Count_Tracker!A:O", countRow);
}

return Promise.all([reportBurndown()])
  .catch(err => {
    console.error(err);
    process.exit(1);
  })
  .then(() => {
    process.exit(0);
  });
